#define FCL_CTRL 0xB00
#define FCL_STATUS 0xB04
#define FCL_IODATA_IN 0xB08
#define FCL_IODATA_OUT 0xB0C
#define FCL_EN 0xB10

#define GPIO_DIRECTION_MODE 0xA04
#define GPIO_OUTPUT_ENABLE 0xA08
#define GPIO_OUTPUT_VALUE 0xA0C
#define GPIO_INPUT_VALUE 0xA10

#define SPRI_CLKOUT 0
#define SPRI_DATAOUT 1
#define SPRI_CONFIG 2
#define SPRI_DONE 3
#define SPRI_XI_SWAP 4
#define SPRI_STATUS 5

#define GPIO_SPRI_DIN 13
#define GPIO_FLASH_CS 12

#define GPIO_BOOTSEL0 15
#define GPIO_BOOTSEL1 14

#define SPI_DELAY 20

#define FLASH_WREN 0x06
#define FLASH_WRDI 0x04
#define FLASH_RDID 0x9F
#define FLASH_RDSR 0x05
#define FLASH_WRSR 0x01
#define FLASH_READ 0x03
#define FLASH_FAST_READ 0x0B
#define FLASH_PP 0x02
#define FLASH_SE 0xD8
#define FLASH_BE 0xC7

#define GENNUM_FLASH 1
#define GENNUM_FPGA  2
#define FPGA_FLASH   3

int rr_init(int a_fd);
void rr_writel(uint32_t data, uint32_t addr);
uint32_t rr_readl(uint32_t addr);
void gennum_writel(uint32_t data, uint32_t addr);
uint32_t gennum_readl(uint32_t addr);
int rr_load_bitstream(const void *data, int size8);
int rr_load_bitstream_from_file(const char *file_name);

void gpio_set1(uint32_t addr, uint8_t bit);
void gpio_set0(uint32_t addr, uint8_t bit);
uint8_t gpio_get(uint32_t addr, uint8_t bit);
void gpio_config(void);
void gpio_bootselect(uint8_t select);
uint8_t flash_read_status(void);
uint32_t flash_read_id(void);
int force_load_fpga_from_flash (void);
int readback_flash(char *filename, uint32_t size, uint32_t addr);
int load_mcs_to_flash(char * filename);
