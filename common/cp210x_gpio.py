#!   /usr/bin/env	python
#    coding: utf8

import fcntl, struct, termios, os
import time
import array

class CCP210x:
	def __init__(self, device):
		self.fd = open(device, 'wb')

	def gpio_set(self, mask):
                f = array.array('I', [mask])
                f[0] = (f[0] << 8) | 0xFF
		fcntl.ioctl(self.fd.fileno(), 0x8001, f, 1)

	def gpio_get(self):
		f = array.array('I', [0])
		fcntl.ioctl(self.fd.fileno(), 0x8000, f, 1)
		return f[0]


# Create an object (the 0 is used to generate the name, eg. /dev/ttyUSB0
#gpio = cp210x_gpio(0)

# Infinite test loop
#while 1:
#	# Pass the mask of the 4 bits as a hex number
#	gpio.gpio_set(0xf)
#	# Returns the states of the 4 bits as hex number
#	print gpio.gpio_get()
#	time.sleep(1)
#	gpio.gpio_set(0x0)
#	print gpio.gpio_get()
#	time.sleep(1)
