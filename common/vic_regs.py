#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2012
# Author: Matthieu Cattin (CERN)
# Licence: GPL v2 or later.
# Website: http://www.ohwr.org
# Last modifications: 8/5/2012


# Vectored interrupt controller registers
VIC_REGS=['VIC registers', {
        'CTL':[0x00, 'VIC control', {
                'EN':[0, 'Enable VIC', 0x1],
                'POL':[1, 'Output polaritity (0=low, 1=high)', 0x1],
                'EMU_EDGE':[2, 'Emulate edge sensitive output', 0x1],
                'EMU_LEN':[3, 'Emulated edge pulse length timer', 0xFFFF],
                'RESERVED':[19, 'Reserved', 0xFFF]}],
        'RISR':[0x04, 'Raw interrupt status', {}],
        'IER':[0x08, 'Interrupt enable', {}],
        'IDR':[0x0C, 'Interrupt disable', {}],
        'IMR':[0x10, 'Interrupt enable mask', {}],
        'VAR':[0x14, 'Vector address', {}],
        'SWIR':[0x18, 'Software interrupt', {}],
        'EOIR':[0x1C, 'End of interrupt acknowledge', {}]
        }]
