#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2013
# Author: Matthieu Cattin <matthieu.cattin@cern.ch>
# Licence: GPL v2 or later.
# Website: http://www.ohwr.org

# Import system modules
import sys
import time
import os

# Add common modules and libraries location to path


# Import common modules
from vv_pts import *


class VME_rr_compatible(VME):

    def __init__(self,lun):
	""" The vmeio driver lun (logical unit).
	    At driver install time, insmod maps lun on to
	    VME (csr, application window, interrupts).
	    Lun is set when creating a VME object
	"""
        cwd = os.path.dirname(__file__)
	self.lib = CDLL(cwd + '/../../../svec_pts/src/lib/libvv_pts.so')
	int = 0
	if type(lun) == type(int):
	    self.lun = lun
	else:
	    self.lun = int
	    raise BusWarning("Warning: VME __init__: Bad lun, default to 0")

    def iread(self, bar, offset, width):
        data = self.vv_read(offset)
        #print("vv_read : offset:0x%4x data:0x%08x"%(offset, data))
        return data

    def iwrite(self, bar, offset, width, datum):
        #print("vv_write: offset:0x%4x data:0x%08x"%(offset, datum))
        return self.vv_write(offset, datum)

