#!   /usr/bin/env   python
#    coding: utf8

# Copyright CERN, 2011
# Author: Matthieu Cattin <matthieu.cattin@cern.ch>
# Licence: GPL v2 or later.
# Website: http://www.ohwr.org
# Last modifications: 16/5/2012

# Import system modules
import sys
import time
import datetime
import os

from fmc_eeprom import *


"""
Tests IPMI formatting library.

"""

def main (default_directory='.'):

    # Constants declaration
    PART_NUMBER = "EDA-02063-V5-0"
    SERIAL = "HCCFFIA___-CR000003"
    EEPROM_BIN_FILENAME = "eeprom_content.out"
    EEPROM_BIN_FILENAME = os.path.join(default_directory, EEPROM_BIN_FILENAME)
    EEPROM_SIZE = 8192 # in Bytes


    #==================================================
    # Serial number
    serial = SERIAL
    print "Board's serial number: %s\n" % serial

    #==================================================
    # Calculate number of minutes since 0:00 1/1/96
    now_date = datetime.datetime.now()
    ref_date = datetime.datetime(1996, 1, 1)
    diff_date = now_date - ref_date
    total_seconds = diff_date.days * 86400 + diff_date.seconds
    current_date = int(total_seconds//60)
    print "Current date/time: %d minutes (since 0:00 1/1/96)\n" % current_date

    # Manufacturiing date = current date
    print "Manufacturing date : %d 0x%06X" % (current_date, current_date)
    mfg_date = current_date

    #==================================================
    # Create Board Info Area
    # FRU field is used to store the date of generation of the eeprom content
    # This could be used later to determine if the content has to be udated (bug fix, ...)
    print "EEPROM content generated: %s\n" % now_date
    fru = "%s" % now_date
    bia = BoardInfoArea(mfg_date, "CERN", "FmcAdc100m14b4cha", serial, PART_NUMBER, fru)

    #==================================================
    # Multirecords Area

    # output number, vnom, vmin, vmax, ripple, imin, imax
    dcload0 = DCLoadRecord(0, 2.5, 2.375, 2.625, 0.0, 0, 4000) # VADJ
    dcload1 = DCLoadRecord(1, 3.3, 3.135, 3.465, 0.0, 0, 3000) # P3V3
    dcload2 = DCLoadRecord(2, 12.0, 11.4, 12.6, 0.0, 0, 1000)  # P12V
    dcload = [ dcload0, dcload1, dcload2 ]

    # output number, vnom, vmin, vmax, ripple, imin, imax
    dcout0 = DCOutputRecord(3, 0.0, 0.0, 0.0, 0.0, 0, 0) # VIO_B_M2C
    dcout1 = DCOutputRecord(4, 0.0, 0.0, 0.0, 0.0, 0, 0) # VREF_A_M2C
    dcout2 = DCOutputRecord(5, 0.0, 0.0, 0.0, 0.0, 0, 0) # VREF_B_M2C
    dcout = [ dcout0, dcout1, dcout2 ]

    # module size  : 0=single width, 1=double width
    # P1 size      : 0=LPC, 1=HPC
    # P2 size      : 0=LPC, 1=HPC, 3=not fitted
    # clock dir    : 0=M2C, 1=C2M
    # nb sig P1 A  : number
    # nb sig P1 B  : number
    # nb sig P2 A  : number
    # nb sig P2 B  : number
    # nb GBT P1    : number
    # nb GBT P2    : number
    # max TCK freq : frequency in MHz
    oem = OEMRecord(0, 1, 3, 1, 68, 0, 0, 0, 0, 0, 0)

    #==================================================
    # Internal Use Area
    # Takes an array of byte as parameter
    iua_data = [0x1,0x2,0x3,0x4]
    iua = InternalUseArea(iua_data)

    #==================================================
    # Write eeprom content to a binary file
    ipmi_open_file(EEPROM_BIN_FILENAME)
    #ipmi_set(bia, dcload, dcout, oem, iua)
    ipmi_set(bia, dcload, dcout, oem)
    ipmi_write()
    ipmi_close_file()

    #==================================================
    # Read eeprom content from binary file
    f_eeprom = open(EEPROM_BIN_FILENAME, "rb")
    eeprom_data = []
    byte = f_eeprom.read(1) # reads one byte
    while byte:
        eeprom_data.append(ord(byte))
        byte = f_eeprom.read(1) # reads one byte
    f_eeprom.close()

    print "Raw EEPROM data:"
    i = 0
    for data in eeprom_data:
        if i%8 == 0:
            print "0x%02X (%3d) : %02X" % (i, i, data),
        else:
            print "%02X" % (data),
        i += 1
        if i%8 == 0:
            print ""

    print("\n")

    dsum = 0
    for data in eeprom_data[158:162]:
        dsum += data
        print("0x%02X  0x%X" % (data, dsum))
    print("\n\nsum: 0x%02X" % dsum)
    checksum = (0xff & (1 + ~dsum))
    print("calculated checksum: 0x%02X" % checksum)
    print("data checksum      : 0x%02X" % eeprom_data[162])
    print("")
    print("check data: 0x%02X" % (dsum + eeprom_data[162]))
    print("check data: 0x%02X" % (dsum + checksum))


if __name__ == '__main__' :
    main()
